package pl.agh.jtp2.thread.date;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class Clock implements Runnable{
    private final JLabel time = new JLabel();
    private JFrame ClockFrame = null;
    private final SimpleDateFormat sdf  = new SimpleDateFormat("hh:mm");
    private int currentSecond;
    private Calendar calendar;
    private String currThread;
    private float RunTime;
    private long StartTime;
    private long EndTime;

    public void run() {
        currThread = Thread.currentThread().getName();
        StartTime = System.currentTimeMillis();
        System.out.println(currThread + " is starting.");
        setUpGui();
    }
    private void setUpGui() {
        ClockFrame = new JFrame();
        JButton CloseButton = new JButton("Close");
        CloseButton.addActionListener(new SleepListener());
        Clock clock = new Clock();
        ClockFrame.getContentPane().add(BorderLayout.SOUTH, CloseButton);
        ClockFrame.setPreferredSize(new Dimension(100, 80));
        ClockFrame.add(clock.time);
        ClockFrame.pack();
        ClockFrame.setLocationRelativeTo(null);
        ClockFrame.setVisible(true);
        clock.startDisplayingTime();
    }
    private void reset(){
        calendar = Calendar.getInstance();
        currentSecond = calendar.get(Calendar.SECOND);
    }
    public void startDisplayingTime(){
        reset();
        Timer timer = new Timer(1000, new ActionListener(){
            public void actionPerformed (ActionEvent e) {
                if(currentSecond == 60) {
                    reset();
                }
                time.setText(String.format("%s:%02d", sdf.format(calendar.getTime()), currentSecond));
                currentSecond++;
            }
        });
        timer.start();
    }

    public float getRunTime() {
        return RunTime;
    }

    public void setRunTime(long time) {
        this.RunTime = time;
    }

    class SleepListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println(currThread + " stopped.");
            EndTime = System.currentTimeMillis();
            setRunTime(EndTime - StartTime);
            ClockFrame.dispose();
        }
    }

    public String getCurrThread() {
        return currThread;
    }
}
