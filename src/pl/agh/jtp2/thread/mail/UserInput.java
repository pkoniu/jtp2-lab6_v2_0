package pl.agh.jtp2.thread.mail;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class UserInput extends JPanel implements ActionListener, Runnable {
    private SendEmail email;
    private JTextField textField;
    private String CurrThread;
    private String CurrSonThread;
    private float CurrSonRunTime;
    private float RunTime;
    private long StartTime;
    private long EndTime;

    public UserInput() {
        super(new GridBagLayout());

        textField = new JTextField(20);
        textField.addActionListener(this);

        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        c.fill = GridBagConstraints.HORIZONTAL;
        add(textField, c);
    }
    public void run() {
        CurrThread = Thread.currentThread().getName();
        StartTime = System.currentTimeMillis();
        System.out.println(CurrThread + " is starting.");
        getUsersEmail();
        System.out.println(CurrThread + " stopped.");
        EndTime = System.currentTimeMillis();
        setRunTime(EndTime - StartTime);
    }
    public void actionPerformed(ActionEvent evt) {
        String TextProvided = textField.getText();
        email = new SendEmail(TextProvided);
        Thread CreateMsgAndSend = new Thread(email);
        CreateMsgAndSend.setName("Sending email thread");
        setCurrSonThread(email.getCurrThread());
        setCurrSonRunTime(email.getRunTime());
        CreateMsgAndSend.start();
    }
    private void createAndShowGUI() {
        JFrame GetInputFrame = new JFrame("Input email");

        GetInputFrame.add(new UserInput());

        GetInputFrame.pack();
        GetInputFrame.setLocationRelativeTo(null);
        GetInputFrame.setVisible(true);
    }
    public void getUsersEmail() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public float getRunTime() {
        return RunTime;
    }

    public void setRunTime(float runTime) {
        RunTime = runTime;
    }

    public String getCurrThread() {
        return CurrThread;
    }

    public String getCurrSonThread() {
        return email.getCurrThread();
    }

    public void setCurrSonThread(String currSonThread) {
        CurrSonThread = currSonThread;
    }

    public float getCurrSonRunTime() {
        return email.getRunTime();
    }

    public void setCurrSonRunTime(float currSonRunTime) {
        CurrSonRunTime = currSonRunTime;
    }
}
