package pl.agh.jtp2.thread.mail;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class SendEmail implements Runnable{
    private String mail;
    private String CurrThread;
    private float RunTime;
    private long StartTime;
    private long EndTime;

    public SendEmail(String msg) {
        mail = msg;
    }
    public void run() {
        CurrThread = Thread.currentThread().getName();
        StartTime = System.currentTimeMillis();
        System.out.println(CurrThread + " is starting.");
        try {
            makeMsgAndSend();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }
    public void makeMsgAndSend() throws EmailException {
        Properties prop = new Properties();
        InputStream input = null;
        String[] PropArray = new String[4];
        int SmtpPort = 465;

        try {
            input = new FileInputStream("lib/email.properties");
            prop.load(input);
            PropArray[0] = prop.getProperty("hostname");
            SmtpPort = Integer.parseInt(prop.getProperty("smtpport"));
            PropArray[1] = prop.getProperty("authlogin");
            PropArray[2] = prop.getProperty("authpass");
            PropArray[3] = prop.getProperty("from");
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

        Email email = new SimpleEmail();
        email.setHostName(PropArray[0]);
        email.setSmtpPort(SmtpPort);
        email.setAuthenticator(new DefaultAuthenticator(PropArray[1], PropArray[2]));
        email.setSSLOnConnect(true);
        email.setFrom(PropArray[3]);
        email.setSubject("Test");
        email.setMsg("Simple test message send from Patryk's threads program.");
        email.addTo(mail);
        email.send();
        EndTime = System.currentTimeMillis();
        System.out.println(CurrThread + " stopped.");
        setRunTime(EndTime - StartTime);
    }

    public float getRunTime() {
        return RunTime;
    }

    public void setRunTime(float runTime) {
        RunTime = runTime;
    }

    public String getCurrThread() {
        return CurrThread;
    }
}
