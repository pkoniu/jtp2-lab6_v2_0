package pl.agh.jtp2.thread.animation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class CircleAnimation implements Runnable {
    private int x = 70;
    private int y = 70;
    private JFrame AnimationFrame;
    private String CurrThread;
    private float RunTime;

    public void run() {
        long StartTime = System.currentTimeMillis();
        CurrThread = Thread.currentThread().getName();
        System.out.println(CurrThread + "is starting.");
        startAnimation();
        System.out.println(CurrThread + "stopped.");
        long EndTime = System.currentTimeMillis();
        setRunTime(EndTime - StartTime);
    }
    public void startAnimation() {
        AnimationFrame = new JFrame();
        SimpleAnimation AnimatedCircle = new SimpleAnimation();
        JButton CloseButton = new JButton("Close");
        CloseButton.addActionListener(new CloseListener());

        AnimationFrame.getContentPane().add(AnimatedCircle);
        AnimationFrame.getContentPane().add(BorderLayout.SOUTH, CloseButton);
        AnimationFrame.setSize(600, 600);
        AnimationFrame.setLocationRelativeTo(null);
        AnimationFrame.setVisible(true);

        for(int i = 0; i < 100; i++) {
            x += 2;
            y += 2;
            AnimatedCircle.repaint();
            try {
                Thread.sleep(25);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public float getRunTime() {
        return RunTime;
    }

    public void setRunTime(long runTime) {
        RunTime = runTime;
    }

    public String getCurrThread() {
        return CurrThread;
    }

    class SimpleAnimation extends JPanel {
        public void paintComponent(Graphics g) {
            g.setColor(Color.black);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            g.setColor(Color.cyan);
            g.fillOval(x, y, 100, 100);
        }
    }

    private class CloseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            AnimationFrame.dispose();
        }
    }
}
