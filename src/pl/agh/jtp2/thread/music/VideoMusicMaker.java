package pl.agh.jtp2.thread.music;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class VideoMusicMaker implements Runnable{
    static JFrame VideoMusicFrame = new JFrame("Music video!");
    static MyDrawPanel ml;
    private String CurrThread;
    private float RunTime;
    private long StartTime;
    private long EndTime;

    public void run() {
        CurrThread = Thread.currentThread().getName();
        StartTime = System.currentTimeMillis();
        System.out.println(CurrThread + " is starting.");
        startPlaying();
    }

    public void setUpGUI() {
        ml = new MyDrawPanel();
        JButton CloseVideoButton = new JButton("Close");
        CloseVideoButton.addActionListener(new CloseListener());
        VideoMusicFrame.setContentPane(ml);
        VideoMusicFrame.getContentPane().add(BorderLayout.SOUTH, CloseVideoButton);
        VideoMusicFrame.setBounds(40, 40, 200, 200);
        VideoMusicFrame.setLocationRelativeTo(null);
        VideoMusicFrame.setVisible(true);
    }

    public void startPlaying() {
        setUpGUI();
        try {
            Sequencer sequence = MidiSystem.getSequencer();
            sequence.open();

            sequence.addControllerEventListener(ml, new int[] {127});

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            int r;
            for (int i = 5; i < 60; i += 4) {
                r = (int) ((Math.random() * 50) + 1);
                track.add(makeEvent(144, 1, r, 100, i));
                track.add(makeEvent(176, 1, 127, 0, i));
                track.add(makeEvent(128, 1, r, 100, i + 2));
            }

            sequence.setSequence(seq);
            sequence.start();
            sequence.setTempoInBPM(150);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
            e.getMessage();
        }
        return event;
    }

    public float getRunTime() {
        return RunTime;
    }

    public void setRunTime(float runTime) {
        RunTime = runTime;
    }

    class MyDrawPanel extends JPanel implements ControllerEventListener {
        boolean msg = false;

        public void controlChange(ShortMessage event) {
            msg = true;
            repaint();
        }

        public void paintComponent(Graphics g) {
            if(msg) {
                g.fillRect(0, 0, this.getWidth(), this.getHeight());

                int r = (int) (Math.random() * 250);
                int gr = (int) (Math.random() * 250);
                int b = (int) (Math.random() * 250);

                g.setColor(new Color(r,gr,b));

                int ht = (int) ((Math.random() * 120) + 10);
                int wth = (int) ((Math.random() * 120) + 10);

                int x = (int) ((Math.random() * 40) + 10);
                int y = (int) ((Math.random() * 40) + 10);

                g.fillRect(x, y, ht, wth);
                msg = false;
            }
        }
    }

    private class CloseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println(CurrThread + " stopped.");
            EndTime = System.currentTimeMillis();
            setRunTime(EndTime - StartTime);
            VideoMusicFrame.dispose();
        }
    }

    public String getCurrThread() {
        return CurrThread;
    }
}
