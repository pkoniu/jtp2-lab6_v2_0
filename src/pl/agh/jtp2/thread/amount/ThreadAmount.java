package pl.agh.jtp2.thread.amount;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;

/**
 * Created by pkoniu on 4/29/2014.
 */
public class ThreadAmount implements Runnable{
    private ArrayList<Integer> ThreadNumber = new ArrayList<Integer>();

    public void run() {
        for(int i = 0; i < 100; i++) {
            try {
                ThreadNumber.add(getAllThreads());
                Thread.sleep(1900);
            } catch (InterruptedException e) {
                e.getMessage();
            }
        }
    }

    public ArrayList<Integer> getThreadNumber() {
        return ThreadNumber;
    }

    private static int getAllThreads() {
        final ThreadMXBean thbean = ManagementFactory.getThreadMXBean();
        int nAlloc = 0;
        nAlloc = thbean.getThreadCount();
        return nAlloc;
    }
}
