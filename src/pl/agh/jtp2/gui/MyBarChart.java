package pl.agh.jtp2.gui;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by pkoniu on 4/29/2014.
 */


public class MyBarChart extends Application{
    private ArrayList<Float> Times;
    private final static String ThreadName1 = "Video thread";
    private final static String ThreadName2 = "I/O thread";
    private final static String ThreadName3 = "Clock thread";
    private final static String ThreadName4 = "Anim. thread";
    private static float RunTime1;
    private static float RunTime2;
    private static float RunTime3;
    private static float RunTime4;

    public void setData() {
        RunTime1 = Times.get(0);
        RunTime2 = Times.get(1);
        RunTime3 = Times.get(2);
        RunTime4 = Times.get(3);
    }

    public void showMe() {
        launch();
    }

    public void start(Stage stage) {
        stage.setTitle("Threads time on processor");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final javafx.scene.chart.BarChart<String,Number> bc =
                new javafx.scene.chart.BarChart<String,Number>(xAxis,yAxis);
        bc.setAnimated(false);
        bc.setTitle("Thread time summary");
        xAxis.setLabel("Threads");
        yAxis.setLabel("Time");

        XYChart.Series series1 = new XYChart.Series();
        series1.getData().add(new XYChart.Data(ThreadName1, RunTime1));
        series1.getData().add(new XYChart.Data(ThreadName2, RunTime2));
        series1.getData().add(new XYChart.Data(ThreadName3, RunTime3));
        series1.getData().add(new XYChart.Data(ThreadName4, RunTime4));

        Scene scene  = new Scene(bc,800,600);
        bc.getData().add(series1);
        stage.setScene(scene);
        stage.show();

        WritableImage image = new WritableImage((int) stage.getWidth(), (int) stage.getHeight());
        scene.snapshot(image);
        File ChartFile = new File("BarChart.png");

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", ChartFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTimes(ArrayList<Float> times) {
        Times = times;
    }
}
