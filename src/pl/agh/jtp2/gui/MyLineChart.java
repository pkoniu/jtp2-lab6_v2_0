package pl.agh.jtp2.gui;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by pkoniu on 4/29/2014.
 */
public class MyLineChart extends Application{
    private static ArrayList<Integer> ThreadsNumber;

    public void setArrayList(ArrayList<Integer> al) {
        ThreadsNumber = new ArrayList<Integer>(al.size());
        ThreadsNumber = al;
    }

    public void showMe() {
        launch();
    }

    public void start(Stage stage) {
        stage.setTitle("Line Chart Sample");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Time [seconds]");
        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);
        lineChart.setAnimated(false);
        lineChart.setTitle("Amount of threads during program runtime");
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName("Threads");
        //populating the series with data

        int i = 2;
        for (Integer aThreadsNumber : ThreadsNumber) {
            series.getData().add(new XYChart.Data(i, aThreadsNumber));
            i = i+2;
        }

        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().add(series);

        stage.setScene(scene);
        stage.show();

        WritableImage image = new WritableImage((int) stage.getWidth(), (int) stage.getHeight());
        scene.snapshot(image);
        File ChartFile = new File("LineChart.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", ChartFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
