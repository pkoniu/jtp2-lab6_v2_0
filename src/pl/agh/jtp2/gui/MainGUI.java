package pl.agh.jtp2.gui;

import pl.agh.jtp2.thread.amount.ThreadAmount;
import pl.agh.jtp2.thread.animation.CircleAnimation;
import pl.agh.jtp2.thread.date.Clock;
import pl.agh.jtp2.thread.mail.UserInput;
import pl.agh.jtp2.thread.music.VideoMusicMaker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by pkoniu on 4/26/2014.
 */

public class MainGUI {
    private JFrame MainGUIFrame;
    private VideoMusicMaker VideoMusicMakerInstance = new VideoMusicMaker();
    private UserInput UserInputInstance = new UserInput();
    private Clock ClockInstance = new Clock();
    private CircleAnimation CircleAnimationInstance = new CircleAnimation();
    private ThreadAmount number = new ThreadAmount();
    private ArrayList<String> ThreadNames;
    private ArrayList<Float> RunTimes;
    private ArrayList<Integer> ThreadNumber;

    public void startProgram() {
        Thread CountThreads = new Thread(number);
        CountThreads.start();
        setUpGui();
    }

    private void setUpGui() {
        MainGUIFrame = new JFrame();
        MainGUIFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel ButtonPanel = new JPanel();
        ButtonPanel.setBackground(Color.black);
        ButtonPanel.setLayout(new BoxLayout(ButtonPanel, BoxLayout.Y_AXIS));

        JPanel LabelPanel = new JPanel();
        LabelPanel.setBackground(Color.orange);
        LabelPanel.setLayout(new BoxLayout(LabelPanel, BoxLayout.Y_AXIS));

        JPanel MainPanel = new JPanel();
        MainPanel.setBackground(Color.cyan);
        MainPanel.setLayout(new BoxLayout(MainPanel, BoxLayout.Y_AXIS));

        JLabel mainLabel = new JLabel("Start and put to sleep multiple threads simple by clicking buttons.");
        JLabel gap = new JLabel(" ");
        JLabel gap1 = new JLabel(" ");
        JLabel gap2 = new JLabel(" ");
        JLabel musicVideoLabel = new JLabel("Create simple melody and visualize it.");
        JLabel emailLabel = new JLabel("Send a simple email to a address provided in next window.");
        JLabel dateLabel = new JLabel("Display current time.");
        JLabel animationLabel = new JLabel("Display a window with a moving circle.");

        JButton StartMusicButton = new JButton("Music");
        StartMusicButton.addActionListener(new MusicListener());

        JButton StartEmailButton = new JButton("Email");
        StartEmailButton.addActionListener(new EmailListener());

        JButton StartDateButton = new JButton("Clock");
        StartDateButton.addActionListener(new DateListener());

        JButton StartAnimButton = new JButton("Animation");
        StartAnimButton.addActionListener(new AnimListener());

        JButton ExitButton = new JButton("Close program & create chart");
        ExitButton.addActionListener(new ExitListener());

        LabelPanel.add(BorderLayout.WEST, musicVideoLabel);
        LabelPanel.add(BorderLayout.WEST, gap);
        LabelPanel.add(BorderLayout.WEST, emailLabel);
        LabelPanel.add(BorderLayout.WEST, gap1);
        LabelPanel.add(BorderLayout.WEST, dateLabel);
        LabelPanel.add(BorderLayout.WEST, gap2);
        LabelPanel.add(BorderLayout.WEST, animationLabel);

        ButtonPanel.add(BorderLayout.EAST, StartMusicButton);
        ButtonPanel.add(BorderLayout.EAST, StartEmailButton);
        ButtonPanel.add(BorderLayout.EAST, StartDateButton);
        ButtonPanel.add(BorderLayout.EAST, StartAnimButton);

        MainPanel.add(BorderLayout.NORTH, mainLabel);

        MainGUIFrame.getContentPane().add(BorderLayout.NORTH, MainPanel);
        MainGUIFrame.getContentPane().add(BorderLayout.WEST, ButtonPanel);
        MainGUIFrame.getContentPane().add(BorderLayout.EAST, LabelPanel);
        MainGUIFrame.getContentPane().add(BorderLayout.SOUTH, ExitButton);

        MainGUIFrame.setSize(438, 250);
        MainGUIFrame.setLocationRelativeTo(null);
        MainGUIFrame.setVisible(true);
    }

    class MusicListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Thread playingVideo = new Thread(VideoMusicMakerInstance);
            playingVideo.setName("Video thread");
            playingVideo.start();
        }
    }

    class EmailListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Thread getInput = new Thread(UserInputInstance);
            getInput.setName("I/O thread");
            getInput.start();
        }
    }

    class DateListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Thread dateThread = new Thread(ClockInstance);
            dateThread.setName("Clock thread");
            dateThread.start();
        }
    }

    class AnimListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            Thread circleThread = new Thread(CircleAnimationInstance);
            circleThread.setName("Anim. thread ");
            circleThread.start();
        }
    }

    class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            showLineChart();
            System.exit(0);
        }
    }

    public void showBarChart() {
        RunTimes = new ArrayList<Float>();

        RunTimes.add(VideoMusicMakerInstance.getRunTime() / 1000);
        RunTimes.add(UserInputInstance.getRunTime() / 1000);
        RunTimes.add(ClockInstance.getRunTime() / 1000);
        RunTimes.add(CircleAnimationInstance.getRunTime() / 1000);

        MyBarChart barchart = new MyBarChart();
        barchart.setTimes(RunTimes);
        barchart.setData();
        barchart.showMe();
    }

    public void showLineChart() {
        ThreadNumber = new ArrayList<Integer>(number.getThreadNumber().size());
        ThreadNumber = number.getThreadNumber();

        MyLineChart LineChart = new MyLineChart();
        LineChart.setArrayList(ThreadNumber);
        LineChart.showMe();
    }
}
