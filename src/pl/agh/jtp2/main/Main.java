package pl.agh.jtp2.main;

import org.apache.commons.mail.EmailException;
import pl.agh.jtp2.gui.MainGUI;

/**
 * Created by pkoniu on 4/26/2014.
 */
public class Main {
    public static void main(String[] args) throws EmailException {
        MainGUI gui = new MainGUI();
        gui.startProgram();
    }
}